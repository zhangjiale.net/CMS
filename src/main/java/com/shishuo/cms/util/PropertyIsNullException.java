package com.shishuo.cms.util;

public class PropertyIsNullException extends Exception {
	public PropertyIsNullException(String msg) {
		super(msg);
	}
}
