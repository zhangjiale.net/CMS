	</section>
	<!-- js placed at the end of the document so the pages load faster -->
	<script src="${basePath}/admin/js/jquery.js"></script>
	<script src="${basePath}/admin/js/bootstrap.min.js"></script>
	<script class="include" type="text/javascript"
		src="${basePath}/admin/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="${basePath}/admin/js/jquery.scrollTo.min.js"></script>
	<script src="${basePath}/admin/js/jquery.nicescroll.js"
		type="text/javascript"></script>
	<script src="${basePath}/admin/js/dropzone.js"></script>
	<script src="${basePath}/admin/js/respond.min.js"></script>

	<!--common script for all pages-->
	<script src="${basePath}/admin/js/common-scripts.js"></script>


</body>
</html>